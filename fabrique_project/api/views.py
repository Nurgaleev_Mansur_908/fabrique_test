import datetime

from rest_framework.response import Response
from rest_framework.views import APIView
from drf_yasg.utils import swagger_auto_schema

from .helpers import send_mailings
from .models import Client, Mailing, Message
from .serializers import ClientSerializer, UpdateClientSerializer, MailingSerializer, UpdateMailingSerializer, \
    MailingsStatisticSerializer


class ClientView(APIView):

    """
    get:Список сущетвующих клиентов\n
    Список сущетвующих клиентов
    post:Добавления нового клиента\n
    Добавления нового клиента
    put:Обновление данных клиента\n
    Обновление данных клиента
    """

    @swagger_auto_schema(responses={200: ClientSerializer(many=True)})
    def get(self, request):
        serialize = Client.objects.all()
        result = ClientSerializer(serialize, many=True).data
        return Response(result)

    @swagger_auto_schema(request_body=ClientSerializer(), responses={201: 'Created'})
    def post(self, request):
        client = ClientSerializer(data=request.data)
        if not client.is_valid():
            return Response({"error": "incorrect data"}, status=400)
        data = request.data
        if Client.objects.filter(phone_number=data.get('phone_number')).exists():
            return Response("Client with this phone number already exist", status=400)
        client.save()

        return Response(status=201)

    @swagger_auto_schema(request_body=UpdateClientSerializer(), responses={204: "No Data"})
    def put(self, request):
        update_client = UpdateClientSerializer(data=request.data)
        if not update_client.is_valid():
            return Response({"error": "incorrect data"}, status=400)
        data = request.data
        if Client.objects.filter(id=data.get('id')).exists():
            Client.objects.filter(id=data.get('id')).update(phone_number=data.get('phone_number'),
                                                            phone_code=data.get('phone_code'),
                                                            tag=data.get('tag'),
                                                            timezone=data.get('timezone'))
            return Response(status=204)
        return Response("Сlient with this id does not exist", status=404)

class ClientDeleteView(APIView):
    """
    delete:Удаление клиента из справочника\n
    Удаление клиента из справочника
    """
    @swagger_auto_schema(responses={200: "OK"})
    def delete(self, request, id):
        if Client.objects.filter(id=id).exists():
            Client.objects.filter(id=id).delete()
            return Response("OK",status=200)
        return Response("Сlient with this id does not exist", status=404)

class MailingView(APIView):

    """
    get:Список сущетвующих рассылок\n
    Список сущетвующих рассылок
    post:Добавление новой рассылки\n
    Добавление новой рассылки
    put:Обновление данных рассылки\n
    Обновление данных рассылки
    """

    @swagger_auto_schema(responses={200: MailingSerializer(many=True)})
    def get(self, request):
        serialize = Mailing.objects.all()
        result = MailingSerializer(serialize, many=True).data
        return Response(result)

    @swagger_auto_schema(request_body=MailingSerializer(), responses={201: 'Created'})
    def post(self, request):
        mailing = MailingSerializer(data=request.data)
        if not mailing.is_valid():
            return Response({"error": "incorrect data"}, status=400)
        mailing = mailing.save()
        if str(mailing.start_time) < str(datetime.datetime.utcnow()) and str(datetime.datetime.utcnow()) < str(mailing.end_time):
            send_mailings(client_filter=request.data.get('client_filter'), text=request.data.get('text'), mailing_id=mailing.id)
        return Response(status=201)

    @swagger_auto_schema(request_body=UpdateMailingSerializer(), responses={204: "No Data"})
    def put(self, request):
        update_mailing = UpdateMailingSerializer(data=request.data)
        if not update_mailing.is_valid():
            return Response({"error": "incorrect data"}, status=400)
        data = request.data
        if Mailing.objects.filter(id=data.get('id')).exists():
            Mailing.objects.filter(id=data.get('id')).update(start_time=data.get('start_time'),
                                                            text=data.get('text'),
                                                            client_filter=data.get('client_filter'),
                                                            end_time=data.get('end_time'))
            return Response(status=204)
        return Response("Mailing with this id does not exist", status=404)

class MailingDeleteView(APIView):
    """
    delete:Удаление рассылки из справочника\n
    Удаление рассылки из справочника
    """
    @swagger_auto_schema(responses={200: "OK"})
    def delete(self, request, id):
        if Mailing.objects.filter(id=id).exists():
            Mailing.objects.filter(id=id).delete()
            return Response("OK",status=200)
        return Response("Mailing with this id does not exist", status=404)

class StatisticView(APIView):
    """
    get:Статистика по рассылкам\n
    Статистика по рассылкам
    """
    @swagger_auto_schema(responses={200: MailingsStatisticSerializer(many=True)})
    def get(self, request):
        mailings = Mailing.objects.all()
        statistic_list = []
        for mailing in mailings:
            message_count = Message.objects.filter(mailing=mailing).count()
            message_send =  Message.objects.filter(mailing=mailing, sending_status=True).count()
            message_not_send =  Message.objects.filter(mailing=mailing, sending_status=False).count()
            d = {'mailing_text': mailing.text, 'client_filter': mailing.client_filter,
                'message_count': message_count, 'message_send': message_send,
                'message_not_send': message_not_send}
            statistic_list.append(MailingsStatisticSerializer(d).data)

        return Response(statistic_list)

