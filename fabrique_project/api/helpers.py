import datetime
import requests
import json
import os

from .models import Client, Message


def send_mailings(client_filter, text, mailing_id):
    phone_code = client_filter.split(sep=", ")[0]
    tag = client_filter.split(sep=", ")[1]
    clients = Client.objects.filter(phone_code=phone_code, tag=tag)
    token = os.getenv('TOKEN')
    headers = {'Authorization': f'Bearer {token}'}
    for client in clients:
        message = Message(sending_time=datetime.datetime.now(), mailing_id=mailing_id, client=client)
        message.save()

        url = f"https://probe.fbrq.cloud/v1/send/{message.id}"
        body = {
            "id": message.id,
            "phone": message.client.phone_number,
            "text": text
        }

        response = requests.post(url=url, data=json.dumps(body), headers=headers)

        if response.status_code == 200:
            message.sending_status = True
            message.save()
            print(message.id)




