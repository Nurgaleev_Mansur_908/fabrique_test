from django.urls import reverse
from rest_framework.test import APITestCase

from .models import Client


class ClientTest(APITestCase):

    def test_create_client(self):
        url = reverse('client')
        data = {
                  "phone_number": "77777777777",
                  "phone_code": "777",
                  "tag": "some_tag",
                  "timezone": "UTC-6"
                }
        response = self.client.post(url, data=data)
        self.assertEqual(201, response.status_code)
        self.assertEqual("77777777777", Client.objects.get(id=1).phone_number)
        response = self.client.post(url, data=data)
        self.assertEqual(400, response.status_code)


