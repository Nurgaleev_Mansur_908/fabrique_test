from django.core.validators import RegexValidator
from django.db import models

class Mailing(models.Model):
    start_time = models.DateTimeField()
    text = models.CharField(max_length=255)
    filter_regex = RegexValidator(regex=r'^\d{3}, [^\s]{1,50}$', message="Filter must be entered in the format: 'phone_code, tag'.")
    client_filter = models.CharField(validators=[filter_regex], max_length=65)
    end_time = models.DateTimeField()

class Client(models.Model):
    phone_regex = RegexValidator(regex=r'^7{1}\d{10}$', message="Phone number must be entered in the format: '7XXXXXXXXXX'.")
    phone_number = models.CharField(validators=[phone_regex], max_length=11)
    phone_code_regex = RegexValidator(regex=r'^\d{3}$', message="Phone code must be entered in the format: 'XXX'.")
    phone_code = models.CharField(validators=[phone_code_regex], max_length=3)
    tag = models.CharField(max_length=50)
    TIMEZONES = ['UTC-11', 'UTC-10', 'UTC-9', 'UTC-8', 'UTC-7', 'UTC-6', 'UTC-5', 'UTC-4', 'UTC-3', 'UTC-2', 'UTC-1',
                      'UTC+0', 'UTC+1', 'UTC+2', 'UTC+3', 'UTC+4', 'UTC+5', 'UTC+6', 'UTC+7', 'UTC+8', 'UTC+9', 'UTC+10', 'UTC+11', 'UTC+12']
    TIMEZONE_CHOICES = ((x, x) for x in TIMEZONES)
    timezone = models.CharField(max_length=32, choices=TIMEZONE_CHOICES, default='UTC+0')

class Message(models.Model):
    sending_time = models.DateTimeField()
    sending_status = models.BooleanField(default=False)
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)

