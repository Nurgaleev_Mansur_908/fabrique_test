from django.urls import path

from . import views

urlpatterns = [
    path("client/", views.ClientView.as_view(), name='client'),
    path("client/<int:id>", views.ClientDeleteView.as_view()),
    path("mailing/", views.MailingView.as_view()),
    path("mailing/<int:id>", views.MailingDeleteView.as_view()),
    path("statistics/", views.StatisticView.as_view()),
]