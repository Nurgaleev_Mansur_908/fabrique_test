from rest_framework import serializers

from .models import Client, Mailing


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = "__all__"

class UpdateClientSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=True)

    class Meta:
        model = Client
        fields = "__all__"


class MailingSerializer(serializers.ModelSerializer):

    class Meta:
        model = Mailing
        fields = "__all__"

class UpdateMailingSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=True)

    class Meta:
        model = Mailing
        fields = "__all__"

class MailingsStatisticSerializer(serializers.Serializer):
    mailing_text = serializers.CharField(max_length=255)
    client_filter = serializers.CharField(max_length=65)
    message_count = serializers.IntegerField()
    message_send = serializers.IntegerField()
    message_not_send = serializers.IntegerField()